from flask import Flask, request, make_response
from kubernetes import client, config

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route("/endpoint")
def test_sa():
    auth_header = request.headers.get("Authorization")
    if auth_header:
        auth_token = auth_header.split(" ")[1]
    else:
        return make_response("No Auth!", 401)

    config.load_incluster_config()
    authApi = client.AuthenticationV1Api()
    tokenReviewSpec = client.V1TokenReviewSpec()
    tokenReviewSpec.token = auth_token
    tokenReview = client.V1TokenReview(spec=tokenReviewSpec)
    api_response = authApi.create_token_review(tokenReview)
    app.logger.info("token {} -> {}".format(auth_token, api_response))
    return "{}".format(api_response)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
