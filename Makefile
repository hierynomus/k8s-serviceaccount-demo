build: docker/server docker/client

docker/%:
	docker build -t local/k8s-sa-$(@F):latest -f Dockerfile-$(@F) .

kube/%/namespace:
	kubectl $(notdir $(@D)) ns serviceaccount-test
	kubens serviceaccount-test

kube/install/%:
	kubectl apply -f $(@F)/k8s-sa-test.yaml

kube/delete/%:
	kubectl delete $(notdir $(@D))` k8s-sa-$(@F)

kube/redeploy-server: kube/delete/deployment/server kube/delete/job/client kube/install/server kube/install/client

