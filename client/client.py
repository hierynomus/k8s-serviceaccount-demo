import requests
from pathlib import Path


SA_TOKEN="/var/run/secrets/kubernetes.io/serviceaccount/token"

path = Path(SA_TOKEN)
token = ""
if path.is_file():
    token = path.read_text()

    headers = {"Authorization": "Bearer {}".format(token)} if token else dict()
print(headers)
r = requests.get("http://k8s-sa-server:5000/endpoint", headers=headers)
print(r.text)
